FROM python:3-onbuild

ADD main.py /
ADD app /
RUN pip install requests
RUN pip install Flask

EXPOSE 5000


CMD python main.py