from flask import Flask, jsonify, request, Response, render_template
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)


@app.route('/instructors')
def get_instructors():
    return jsonify(data['instructors'])

##INSTRUCTORS PART


@app.route('/instructors/<int:id>')
def get_instructor(id):
    return jsonify(data['instructors'][id])


##MEETINGS PART

@app.route('/meeting')
def get_meeting():
    return jsonify(data['meeting'])

@app.route('/meeting/days')
def get_days():
    return jsonify(data['meeting']['days'])


##ASSIGNMENTS PART
    

@app.route('/assignments', methods = ["GET", "POST"])
def post_assignments():
    if request.method == "GET":
        return jsonify(data['assignments'])
    elif request.method == "POST":
        the_data = request.json
        the_dictionary = data['assignments']
        the_dictionary.append(the_data)
        return Response(the_data, status=200, mimetype='application/json')
        
@app.route('/assignments/<int:id>')
def get_assignment(id):
    return jsonify(data['assignments'][id])

@app.route('/assignments/<int:id>/url')
def get_assignment_url(id):
    return jsonify(data['assignments'][id]['url'])     
        
        
        
